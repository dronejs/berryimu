import { LSM9DS0 } from '../LSM9DS0';
import { I2cBus } from 'i2c-bus';


export class Accelometer {

    bus: I2cBus;
    private address: number;

    constructor(bus: I2cBus) {
        this.bus = bus;
        this.address = LSM9DS0.ACC_ADDRESS;

        this.write(LSM9DS0.CTRL_REG1_XM, 0b01100111);
        this.write(LSM9DS0.CTRL_REG2_XM, 0b00100000);
    }

    write(register,value) {
        return this.bus.writeByteSync(this.address, register, value);
    }

    private read(accL, accH) {
        const acc_l = this.bus.readByteSync(this.address, accL);
        const acc_h = this.bus.readByteSync(this.address, accH);
        const acc_combined = (acc_l | acc_h <<8);

        return acc_combined < 32768 ? acc_combined : acc_combined - 65536;
    }

    readX() {
        return this.read(LSM9DS0.OUT_X_L_A, LSM9DS0.OUT_X_H_A);
    }

    readY() {
        return this.read(LSM9DS0.OUT_Y_L_A, LSM9DS0.OUT_Y_H_A);
    }

    readZ() {
        return this.read(LSM9DS0.OUT_Z_L_A, LSM9DS0.OUT_Z_H_A);
    }



}


