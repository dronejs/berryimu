import socket from './utils/SocketService';
import { measureTilt } from './Orientation';


setInterval(() => {
  socket.postMessage('tilt', measureTilt());
}, 20);