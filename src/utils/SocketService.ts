import SocketConnection from './SocketConnection';

let socket = null;

if (!socket) {
  socket = new SocketConnection();
}

export default socket;