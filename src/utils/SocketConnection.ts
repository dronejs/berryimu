import * as express from "express";
import * as http from "http";
import * as socketIo from "socket.io";


export default class SocketConnection {

  public PORT;
  public app: any;
  private server: any;
  private io: any;
  private port: number;
  private listeners: any[];

  constructor(PORT: number = 8090) {

    this.PORT = PORT;
    this.app = express();
    this.server = http.createServer(this.app);
    this.port = process.env.PORT || this.PORT;
    this.io = socketIo(this.server);
    this.listen();
  }

  postMessage(command, payload) {
    this.io.emit(command, payload);
  }

  private listen(): void {
    this.server.listen(this.port, () => {
      console.log(`Running server on port ${this.port}`);
    });

    this.io.on('connect', (socket: any) => {
      console.log('Connected client on port %s.', this.port);
      socket.on('disconnect', () => {
        console.log('Client disconnected');
      });
    });
  }

}