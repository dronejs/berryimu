

let Q_angle = 0.02;
let Q_gyro = 0.0015;
let R_angle = 0.005;
let y_bias = 0.0;
let x_bias = 0.0;
let XP_00 = 0.0;
let XP_01 = 0.0;
let XP_10 = 0.0;
let XP_11 = 0.0;
let YP_00 = 0.0;
let YP_01 = 0.0;
let YP_10 = 0.0;
let YP_11 = 0.0;
let KFangleX = 0.0;
let KFangleY = 0.0;

export function kalmanFilterY( accAngle, gyroRate, DT) {
    let y;
    let S;

    KFangleY = KFangleY + DT * (gyroRate - y_bias);

    YP_00 = YP_00 + ( - DT * (YP_10 + YP_01) + Q_angle * DT );
    YP_01 = YP_01 + ( - DT * YP_11 );
    YP_10 = YP_10 + ( - DT * YP_11 );
    YP_11 = YP_11 + ( + Q_gyro * DT );

    y = accAngle - KFangleY;
    S = YP_00 + R_angle;

    let K_0 = YP_00 / S;
    let K_1 = YP_10 / S;

    KFangleY = KFangleY + ( K_0 * y );
    y_bias = y_bias + ( K_1 * y );

    YP_00 = YP_00 - ( K_0 * YP_00 );
    YP_01 = YP_01 - ( K_0 * YP_01 );
    YP_10 = YP_10 - ( K_1 * YP_00 );
    YP_11 = YP_11 - ( K_1 * YP_01 );

    return KFangleY
}


export function kalmanFilterX ( accAngle, gyroRate, DT) {
    let x;
    let S;



    KFangleX = KFangleX + DT * (gyroRate - x_bias);

    XP_00 = XP_00 + ( - DT * (XP_10 + XP_01) + Q_angle * DT );
    XP_01 = XP_01 + ( - DT * XP_11 );
    XP_10 = XP_10 + ( - DT * XP_11 );
    XP_11 = XP_11 + ( + Q_gyro * DT );

    x = accAngle - KFangleX;
    S = XP_00 + R_angle;
    let K_0 = XP_00 / S;
    let K_1 = XP_10 / S;

    KFangleX = KFangleX + ( K_0 * x );
    x_bias = x_bias + ( K_1 * x );

    XP_00 = XP_00 - ( K_0 * XP_00 );
    XP_01 = XP_01 - ( K_0 * XP_01 );
    XP_10 = XP_10 - ( K_1 * XP_00 );
    XP_11 = XP_11 - ( K_1 * XP_01 );

    return KFangleX
}
